Dockerfile for SDCC compiler environment with stm8S & stm8L SPL patched   
-----------------------------------------------------------------------   
   
Thanks Radek Sevcik & Georg Icking-Konert for their great jobs! And their original works are as follows:   
  - https://github.com/zcsevcik/docker-stm8s   
  - https://github.com/gicking/STM8-SPL_SDCC_patch   
   
Integrated stm8S & stm8L SPL and patched   
  - STM8S/A: "STSW-STM8069" with STM8S_StdPeriph_Lib_sdcc.patch   
  - STM8L15x-16x-05x-AL31-L: "STSW-STM8016" STM8L15x-16x-05x-AL31-L_StdPeriph_Lib_sdcc.patch   
  - both 2 SPLs are in the dictionary '\opt\STM8LSPL' & 'STM8SSPL'   
   
Patch technical status from Georg Icking-Konert   
  - applied SDCC specific changes to headers and sources (changes marked with "SDCC patch“)   
  - changed ISR headers in all examples to skip ISR declaration (see open points)   
  - added read/write-routines for >64kB memory space via inline assembly (thanks Philipp!)   
  - created a SDCC "template project“ (i.e. Makefile). See $(SPL)/Project/STM8*_StdPeriph_Template/   
  - created Win, Linux, and MacOSX batch scripts to compile and upload via https://github.com/gicking/STM8_serial_flasher   
  - added Doxygen input for creating manual from sources, since provided UM is in Windows proprietary .chm format —> run Doxy to create HTML docu   
  - all SPL examples should compile but require editing of Makefile. Functionality tested partly (see open points)   
   
To Use the Dockerfile   
  - download all files as they will be copied into alpine   
  - docker build Dockerfile   

